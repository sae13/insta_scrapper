import concurrent.futures
import json
import logging
import pickle
import re
from datetime import datetime
from pathlib import Path
from shutil import move

from requests import get, post, Session
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.media import UploadFile
from wordpress_xmlrpc.methods.posts import NewPost
from wordpress_xmlrpc.methods.users import GetUserInfo

from configs import Configs

logging.basicConfig(format='%(asctime)s %(message)s', level=Configs.log_level)


class Instagram:
    _password = Configs.Instagram.password
    username = Configs.Instagram.username
    session = Session()

    def __init__(self, username=None, password=None):
        if username is not None:
            self.username = username
        if password is not None:
            self._password = password
        Path(f'data/{self.username}').mkdir(exist_ok=True, parents=True)
        self.cookie_file = f'data/{self.username}/cookie.json'
        if Path(self.cookie_file).exists():
            if not self._login_with_cookie():
                self._login_with_user_and_password()
        else:
            self._login_with_user_and_password()
        print(self)

    def _login_with_user_and_password(self):
        time = int(datetime.now().timestamp())
        response = get(Configs.Instagram.main_login_url)
        csrf = response.cookies['csrftoken']

        payload = {
            'username': self.username,
            'enc_password': f'#PWD_INSTAGRAM_BROWSER:0:{time}:{self._password}',
            'queryParams': {},
            'optIntoOneTap': 'false'
        }

        login_header = {
            "User-Agent":
                "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko)"
                "Chrome/77.0.3865.120 Safari/537.36",
            "X-Requested-With": "XMLHttpRequest",
            "Referer": f"{Configs.Instagram.instagram_url}/accounts/login/",
            "x-csrftoken": csrf
        }

        login_response = post(Configs.Instagram.login_url, data=payload, headers=login_header)
        json_data = json.loads(login_response.text)

        if json_data["authenticated"]:

            print("login successful")
            cookies = login_response.cookies
            self.cookie_jar = cookies.get_dict()
            with open(self.cookie_file, 'w') as cookie_file:
                cookie_file.write(json.dumps(self.cookie_jar))
                cookie_file.close()
            csrf_token = self.cookie_jar['csrftoken']
            for name, value in self.cookie_jar.items():
                self.session.cookies.set(name, value)
            print("csrf_token: ", csrf_token)
            session_id = self.cookie_jar['sessionid']
            print("session_id: ", session_id)
        else:
            print("login failed ", login_response.text)

    def _login_with_cookie(self):
        with open(self.cookie_file, 'r') as cookie_file:
            self.cookie_jar = json.loads(cookie_file.read())
            for name, value in self.cookie_jar.items():
                self.session.cookies.set(name, value)
            cookie_file.close()
        try:
            self.session.get(
                f'{Configs.Instagram.instagram_url}/{self.username}?{Configs.Instagram.ask_for_json}').json()
            return True
        except Exception as e:
            return False

    def _get_next_page_of_user(self, user_id, max_id):
        url = f'{Configs.Instagram.instagram_url}/graphql/query?{Configs.Instagram.ask_for_json}' \
              f'&query_id={Configs.Instagram.query_id}&id={user_id}&first=12&after={max_id}'
        return self.session.get(url).json()

    def get_all_user_page_links(self, username):
        data = self.session.get(f'{Configs.Instagram.instagram_url}/{username}?{Configs.Instagram.ask_for_json}').json()
        posts = {}
        '''
        ss['graphql']['user']['edge_owner_to_timeline_media']['edges'][0]['node']['shortcode']
        ss['graphql']['user']['edge_owner_to_timeline_media']['edges'][0]['node']['id']
        ss['graphql']['user']['edge_owner_to_timeline_media']['page_info']['has_next_page']
        ss['graphql']['user']['edge_owner_to_timeline_media']['count']
        '''
        count = data['graphql']['user']['edge_owner_to_timeline_media']['count']
        user_id = data['graphql']['user']['id']
        max_id = data['graphql']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
        for _post in data['graphql']['user']['edge_owner_to_timeline_media']['edges']:  # [0]['node']['shortcode']:
            posts[_post['node']['id']] = _post['node']['shortcode']

        while len(posts) < count:
            data = self._get_next_page_of_user(user_id, max_id)
            max_id = data['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
            for _post in data['data']['user']['edge_owner_to_timeline_media']['edges']:  # [0]['node']['shortcode']:
                posts[_post['node']['id']] = _post['node']['shortcode']
        user_data_path = Path(f'data/posts/{username}/')
        user_data_path.mkdir(parents=True, exist_ok=True)
        file_path = Path(f'{user_data_path.as_posix()}/posts_id_src.json')
        if file_path.exists():
            old_dir = Path(f'{user_data_path.as_posix()}/old/')
            old_dir.mkdir(exist_ok=True)
            move(file_path.as_posix(), f'{old_dir.as_posix()}/posts_id_src{int(datetime.now().timestamp())}.json')
        with open(file_path.as_posix(), 'w') as f:
            f.write(json.dumps(posts))
            f.close()
        return posts

    def _save_edge_to_path(self, path, edge_node):
        is_video = edge_node['is_video']
        url = edge_node['video_url'] if is_video else edge_node['display_url']
        name = f'{edge_node["id"]}__{edge_node["shortcode"]}'
        name = f'{name}.mp4' if is_video else f'{name}.jpeg'
        media = self.session.get(url).content
        save_path = f'{path}/images/{name}.jpg' if not is_video else f'{path}/videos/{name}.mp4'
        with open(save_path, 'wb') as f:
            f.write(media)
            f.close()

    def download_post_by_link(self, src):
        user_post = self.session.get(
            f'{Configs.Instagram.instagram_url}/p/{src}?{Configs.Instagram.ask_for_json}').json()
        user_name = user_post['graphql']['shortcode_media']['owner']['username']
        path_dir = f'data/posts/{user_name}/{src}'
        Path(path_dir).mkdir(exist_ok=True, parents=True)
        if Path(f'{path_dir}/info.json').exists():
            logging.info(f"instagram post:{src} already exists. skiping ...\n")
            return user_post
        contents = ''
        for content in user_post['graphql']['shortcode_media']['edge_media_to_caption']['edges']:
            contents += f'{content["node"]["text"]}\n'
        feature_image = self.session.get(user_post['graphql']['shortcode_media']['display_url']).content
        Path(f'{path_dir}/images').mkdir(exist_ok=True)
        Path(f'{path_dir}/videos').mkdir(exist_ok=True)
        with open(f'{path_dir}/content.txt', 'w') as cn:
            cn.write(contents)
            cn.close()
        with open(f'{path_dir}/feature_image.jpeg', 'wb') as f:
            f.write(feature_image)
            f.close()
        with open(f'{path_dir}/info.json', 'w') as f:
            f.write(json.dumps(user_post))
            f.close()

        with concurrent.futures.ThreadPoolExecutor() as e:
            futures = [e.submit(self._save_edge_to_path, path_dir, edge['node']) for edge in
                       user_post['graphql']['shortcode_media']['edge_sidecar_to_children']['edges']]
        concurrent.futures.wait(futures)
        logging.info(f"instagram post {path_dir.title()} is saving\n")

    def save_all_user_posts(self, username):
        posts_path = f'data/posts/{username}/posts_id_src.json'
        if not Path(posts_path).exists():
            self.get_all_user_page_links(username)
        with open(posts_path, 'r') as j:
            data: dict = json.loads(j.read())
            j.close()
        with concurrent.futures.ThreadPoolExecutor(5) as e:
            [e.submit(self.download_post_by_link, v) for v in data.values()]


class WordPress:
    url = f'{Configs.WordPress.url}/{Configs.WordPress.xml_rpc_name}'
    username = Configs.WordPress.user
    password = Configs.WordPress.password

    def __init__(self, url=None, username=None, password=None):
        if url is not None:
            self.url = url
        if username is not None:
            self.username = username
        if password is not None:
            self.password = password
        self.client = Client(self.url, self.username, self.password)
        logging.info(f"wordpress user login: {self.client.call(GetUserInfo())}")

    def send_instagram_post_to_wp(self, path: Path):
        already_posted = list(path.glob('wp.pickle'))
        if len(already_posted) > 0:
            return pickle.load(open(already_posted.pop().as_posix(), 'rb'))
        feature_image_list = list(path.glob('feature_image.jpeg'))
        content_list = list(path.glob('content.txt'))
        info_list = list(path.glob('info.json'))
        video_list = list(path.glob('videos/*.mp4'))
        image_list = list(path.glob('images/*.jpg'))
        if len(feature_image_list) == 0 or len(content_list) == 0 or len(info_list) == 0:
            raise FileNotFoundError(f"feature_image.jpeg or content.txt not found for {path.absolute().as_posix()}")
        feature_image = feature_image_list.pop()
        content = content_list.pop().read_text()
        info = json.loads(info_list.pop().read_text())
        media_list = [
            dict(name=f'{path.name}.jpg', type='image/jpeg', bits=open(feature_image, 'rb').read())
        ]
        for image in image_list:
            media_list.append(dict(name=image.name, type='image/jpeg', bits=open(image, 'rb').read()))
        for video in video_list:
            media_list.append(dict(name=video.name, type='video/mp4', bits=open(video, 'rb').read()))
        futures = []
        for media in media_list:
            futures.append(self.client.call(UploadFile(media)))

        for f in futures:
            if f['type'] == 'image/jpeg':
                content += f'<figure class="wp-block-image alignwide size-large">' \
                           f'<img src="{f["url"]}"  class="wp-image-6" /></figure><br>\n'
            else:
                content += f'<figure class="wp-block-video alignfull">' \
                           f'<video controls src="{f["url"]}"></video></figure><br>\n'
        tags_regex = re.compile(
            "#[_\u0622\u0627\u0628\u067E\u062A-\u062C\u0686\u062D-\u0632\u0698\u0633"
            "-\u063A\u0641\u0642\u06A9\u06AF\u0644-\u0648\u06CC]+")
        tags = tags_regex.findall(content)

        post: WordPressPost = WordPressPost()
        post.title = content.split("\n")[0]
        post.content = content
        post.post_status = 'publish'
        post.date = info['graphql']['shortcode_media']['taken_at_timestamp']
        post.thumbnail = futures[0]['attachment_id']
        post.terms_names = {
            'post_tag': [],
            'category': []
        }
        for tag in tags:
            tag = tag[1:].replace("_", "‌")
            post.terms_names['post_tag'].append(tag)
            post.terms_names['category'].append(tag)
        post.id = self.client.call(NewPost(post))
        with open(f'{path.as_posix()}/wp.pickle', 'wb') as wp:
            pickle.dump(post, wp)
            wp.close()
        logging.info(f"wordpress post:{post.id}=>{post.title} saved")
        return post.struct

    def send_all_insta_user_posts_to_blog(self, username):
        path = Path(f'data/posts/{username}')
        if not path.exists():
            instagram = Instagram()
            instagram.save_all_user_posts(username)
        for dir in path.iterdir():
            if dir.is_dir():
                self.send_instagram_post_to_wp(dir)


if __name__ == '__main__':
    # inst = Instagram()
    # inst.save_all_user_posts('shahrdari.shahrbabak')
    wp = WordPress()
    wp.send_all_insta_user_posts_to_blog('shahrdari.shahrbabak')
    # inst.download_post_by_link("CWiPQ35oPOd")
    # a = inst.get_all_user_page_links("shahrdari.shahrbabak")
    # ss = inst.session.get(
    #     f'{inst._instagram_url}/{"shahrdari.shahrbabak"}?{inst._ask_for_json}&max_id=2699674052961870087_47646893186').json()
