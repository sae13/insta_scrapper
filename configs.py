from os import getenv


class Configs:
    log_level = int(getenv('ins_log', 30))  # debug 10    error 40

    class Instagram:
        username = getenv("ig_username", 'username')
        password = getenv("ig_password", 'password')
        ask_for_json = '__a=1'
        instagram_url = 'https://instagram.com'
        main_login_url = 'https://www.instagram.com/accounts/login/'
        login_url = 'https://www.instagram.com/accounts/login/ajax/'
        query_id = '17888483320059182'

    class WordPress:
        url = getenv('wp_url', 'https://site.ir')
        xml_rpc_name = getenv('wp_xmp_rpc', 'xmlrpc.php')
        user = getenv('wp_user', 'user')
        password = getenv('wp_password', 'password')
